Framework '4.0'

properties {
    $name = "EmployeeDirectory"
    $birthYear = 2015
    $company = "Headspring"
    $configuration = 'Release'
    $src = resolve-path '.\src'
    $projects = @(gci $src -rec -filter *.csproj)

    $build = if ($env:build_number -ne $NULL) { $env:build_number } else { '0' }
    $version = [IO.File]::ReadAllText('.\VERSION.txt') + '.' + $build

    $roundhouse_dir = "$src\packages\roundhouse.0.8.6\bin"
    $roundhouse_output_dir = "$roundhouse_dir\output"
    $roundhouse_exe_path = "$roundhouse_dir\rh.exe"
    $db_scripts_dir ="$src\EmployeeDirectory.Core\DatabaseMigrations"
    $roundhouse_version_file = "$src\EmployeeDirectory.core\bin\$configuration\EmployeeDirectory.Core.dll"
}

task default -depends Compile, RebuildDevDatabase, RebuildTestDatabase, Test
task dev -depends Compile, UpdateDevDatabase, UpdateTestDatabase, Test
task ci -depends Compile, RebuildTestDatabase, Test

task help {
   Write-Help-Header
   Write-Help-For-Alias "(default)" "Intended for first build or when you want a fresh, clean local copy."
   Write-Help-For-Alias "dev" "Optimized for local dev; Most notably UPDATES databases instead of REBUILDING."
   Write-Help-For-Alias "ci" "Rebuilds test database from scratch. Used by TeamCity"
   Write-Help-Footer
   exit 0
}

task Test -depends Compile {
    $testRunners = @(gci $src\packages -rec -filter Fixie.Console.exe)

    if ($testRunners.Length -ne 1)
    {
        throw "Expected to find 1 Fixie.Console.exe, but found $($testRunners.Length)."
    }

    foreach ($project in $projects)
    {
        $projectName = [System.IO.Path]::GetFileNameWithoutExtension($project)

        if ($projectName.EndsWith("Tests"))
        {
            $testRunner = $testRunners[0].FullName
            $testAssembly = "$($project.Directory)\bin\$configuration\$projectName.dll"
            exec { & $testRunner $testAssembly }
        }
    }
}

task Compile -depends AssemblyInfo {
  exec { msbuild /t:clean /v:q /nologo /p:Configuration=$configuration /p:VisualStudioVersion=12.0 $src\$name.sln }
  exec { msbuild /t:build /v:q /nologo /p:Configuration=$configuration /p:VisualStudioVersion=12.0 $src\$name.sln }
}


task RebuildDevDatabase{
    deploy-database "Rebuild" ".\SqlExpress" $name "DEV"
}

task RebuildTestDatabase {
    deploy-database "Rebuild" ".\SqlExpress" "$name-Test" "TEST"
}

task UpdateDevDatabase {
    deploy-database "Update" ".\SqlExpress" $name "DEV"
}

task UpdateTestDatabase {
    deploy-database "Update" ".\SqlExpress" "$name-Test" "TEST"
}

function deploy-database($action, $server, $database, $env) {
	if (!$env) {
		$env = "LOCAL"
		Write-Host "RoundhousE environment variable is not specified... defaulting to 'LOCAL'"
	} else {
		Write-Host "Executing RoundhousE for environment:" $env
	}

	$connection_string = "Server=$server;Database=$database;Trusted_Connection=True;"

    Write-host $action
	if ($action -eq "Update") {
        Write-Host "Entered Update action statement"
		exec { & $roundhouse_exe_path --connectionstring $connection_string `
                                      --commandtimeout 300 `
                                      --env $env `
                                      --output $roundhouse_output_dir `
                                      --sqlfilesdirectory $db_scripts_dir `
                                      --versionfile $roundhouse_version_file `
                                      --silent `
                                      --transaction }
	}
	if ($action -eq "Rebuild") {
        Write-Host "Entered Rebuild action statement"
		exec { & $roundhouse_exe_path --connectionstring $connection_string `
                                      --commandtimeout 300 `
                                      --env $env `
                                      --output $roundhouse_output_dir `
                                      --silent `
                                      --drop }

		exec { & $roundhouse_exe_path --connectionstring $connection_string `
                                      --commandtimeout 300 `
                                      --env $env `
                                      --output $roundhouse_output_dir `
                                      --sqlfilesdirectory $db_scripts_dir `
                                      --versionfile $roundhouse_version_file `
                                      --silent `
                                      --transaction `
                                      --simple }
	}
}


task AssemblyInfo {
    $copyright = get-copyright

    foreach ($project in $projects) {
        $projectName = [System.IO.Path]::GetFileNameWithoutExtension($project)

        regenerate-file "$($project.DirectoryName)\Properties\AssemblyInfo.cs" @"
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: ComVisible(false)]
[assembly: AssemblyProduct("$name")]
[assembly: AssemblyTitle("$projectName")]
[assembly: AssemblyVersion("$version")]
[assembly: AssemblyFileVersion("$version")]
[assembly: AssemblyCopyright("$copyright")]
[assembly: AssemblyCompany("$company")]
[assembly: AssemblyConfiguration("$configuration")]
"@
    }
}

function get-copyright {
    $date = Get-Date
    $year = $date.Year
    $copyrightSpan = if ($year -eq $birthYear) { $year } else { "$birthYear-$year" }
    return "Copyright � $copyrightSpan $maintainers"
}

function regenerate-file($path, $newContent) {
    $oldContent = [IO.File]::ReadAllText($path)

    if ($newContent -ne $oldContent) {
        $relativePath = Resolve-Path -Relative $path
        write-host "Generating $relativePath"
        [System.IO.File]::WriteAllText($path, $newContent, [System.Text.Encoding]::UTF8)
    }
}

# ------------
# Help Section
# ------------

function Write-Help-Header($description) {
   Write-Host ""
   Write-Host "********************************" -foregroundcolor DarkGreen -nonewline;
   Write-Host " HELP " -foregroundcolor Green  -nonewline; 
   Write-Host "********************************"  -foregroundcolor DarkGreen
   Write-Host ""
   Write-Host "This build script has the following common build " -nonewline;
   Write-Host "task " -foregroundcolor Green -nonewline;
   Write-Host "aliases set up:"
}

function Write-Help-For-Alias($alias,$description) {
   Write-Host "  > " -nonewline;
   Write-Host "$alias" -foregroundcolor Green -nonewline; 
   Write-Host " = " -nonewline; 
   Write-Host "$description"
}

function Write-Help-Footer($description) {
   Write-Host ""
   Write-Host " For a complete list of build tasks, view default.ps1."
   Write-Host ""
   Write-Host "**********************************************************************" -foregroundcolor DarkGreen
}