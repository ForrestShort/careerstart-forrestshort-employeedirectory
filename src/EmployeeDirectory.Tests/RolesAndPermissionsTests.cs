﻿using EmployeeDirectory.Core;
using EmployeeDirectory.Features.Employee;

namespace EmployeeDirectory.Tests
{
    public class RolesAndPermissionsTests : Tests
    {
        public void SystemAdministrator(Employee systemAdmin, Employee hr, Employee manager, Role systemAdminRole, Role hrRole, Role managerRole)
        {

            systemAdminRole.RoleName = "System Administrator";
            hrRole.RoleName = "Human Resources";
            managerRole.RoleName = "Manager";

            Save(systemAdmin, hr, manager, systemAdminRole, hrRole, managerRole, new EmployeeRole { Employee = systemAdmin, Role = systemAdminRole}, new EmployeeRole { Employee = hr, Role = hrRole },
                new EmployeeRole { Employee = manager, Role = managerRole});
            
            Send(new SetRolesForm
            {
                Id = systemAdmin.Id,
                EmployeeName = systemAdmin.FirstName + " " + systemAdmin.LastName,
                Roles = new[]
                {
                    new RolesSelection
                    {
                        RoleId = systemAdminRole.Id,
                        Name = systemAdminRole.RoleName,
                        Selected = true
                    },
                    new RolesSelection
                    {
                        RoleId = hrRole.Id,
                        Name = hrRole.RoleName,
                        Selected = true
                    },
                    new RolesSelection
                    {
                        RoleId = managerRole.Id,
                        Name = managerRole.RoleName,
                        Selected = false
                    }
                }
            });

            
            var setRolesForm = Send(new SetRolesQuery{ Id = systemAdmin.Id });
            
            setRolesForm.Roles[0].Name.ShouldBe(hrRole.RoleName);
            setRolesForm.Roles[0].RoleId.ShouldBe(hrRole.Id);
            setRolesForm.Roles[0].Selected.ShouldBe(true);

            setRolesForm.Roles[1].Name.ShouldBe(managerRole.RoleName);
            setRolesForm.Roles[1].RoleId.ShouldBe(managerRole.Id);
            setRolesForm.Roles[1].Selected.ShouldBe(false);

            setRolesForm.Roles[2].Name.ShouldBe(systemAdminRole.RoleName);
            setRolesForm.Roles[2].RoleId.ShouldBe(systemAdminRole.Id);
            setRolesForm.Roles[2].Selected.ShouldBe(true);
            


        }
    }
}