﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using EmployeeDirectory.App_Start;
using Fixie;
using Ploeh.AutoFixture.Kernel;
using Respawn;
using Fixture = Ploeh.AutoFixture.Fixture;

namespace EmployeeDirectory.Tests
{
    public class TestingConvention : Convention
    {
        private static readonly string ConnectionString =
            ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

        public TestingConvention()
        {
            var checkpoint = new Checkpoint
            {
                SchemasToExclude = new[] {"RoundhousE"},
                TablesToIgnore = new[] {"sysdiagrams"}
            };

            Classes
                .NameEndsWith("Tests");

            Methods
                .Where(method => method.IsVoid() || method.IsAsync());

            ClassExecution
                .CreateInstancePerCase()
                .UsingFactory(type =>
                {
                    checkpoint.Reset(ConnectionString);

                    TestDependencyScope.Begin();

                    return TestDependencyScope.Resolve(type);
                })
                .Wrap<InitializeAutoMapper>();

            FixtureExecution
                .Wrap((fixture, next) =>
                {
                    next();

                    TestDependencyScope.End();
                });

            Parameters
                .Add<AutoFixtureParameterSource>();
        }

        private class InitializeAutoMapper : ClassBehavior
        {
            public void Execute(Class context, Action next)
            {
                AutoMapperBootstrapper.Initialize();
                next();
            }
        }

        private class AutoFixtureParameterSource : ParameterSource
        {
            public IEnumerable<object[]> GetParameters(MethodInfo method)
            {
                var parameterTypes = method.GetParameters().Select(x => x.ParameterType);

                var builder = new Fixture();
                var specimenContext = new SpecimenContext(builder);

                var arguments = parameterTypes.Select(specimenContext.Resolve).ToArray();

                return new[] {arguments};
            }
        }
    }
}