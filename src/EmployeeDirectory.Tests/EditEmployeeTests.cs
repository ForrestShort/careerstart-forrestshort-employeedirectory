﻿using System.Linq;
using EmployeeDirectory.Core;
using EmployeeDirectory.Features.Employee;
using EmployeeDirectory.Infrastructure;
using FluentValidation.TestHelper;
using Should;

namespace EmployeeDirectory.Tests
{
    public class EditEmployeeTests : Tests
    {
        public void EditMyself(Employee myself, EditEmployeeForm editForm)
        {
            Save(myself);
            LoginAs(myself);
            var stub = new StubAuthentication();
            Inject<IAuthenticationService>(stub);

            editForm.Id = myself.Id;

            Send(editForm);
            Query(x => x.Employees).ToList().Count.ShouldEqual(1);

            var actual = Query(x => x.Employees).Single(x => x.Id == myself.Id);

            actual.Username.ShouldEqual(editForm.Username);
            actual.FirstName.ShouldEqual(editForm.FirstName);
            actual.LastName.ShouldEqual(editForm.LastName);
            actual.Title.ShouldEqual(editForm.Title);
            actual.Location.ShouldEqual(editForm.Location);
            actual.Email.ShouldEqual(editForm.Email);
            actual.PhoneNumber.ShouldEqual(editForm.PhoneNumber);

            stub.AuthenticateUsername.ShouldEqual(editForm.Username);
        }

        public void EditOtherUser(Employee myself, Employee employeeToEdit, EditEmployeeForm editForm)
        {
            Save(myself, employeeToEdit);
            
            LoginAs(myself);
            var stub = new StubAuthentication();
            Inject<IAuthenticationService>(stub);

            editForm.Id = employeeToEdit.Id;

            Send(editForm);

            Query(x => x.Employees).ToList().Count.ShouldEqual(2);

            var actual = Query(x => x.Employees).Single(x => x.Id == employeeToEdit.Id);

            actual.Username.ShouldEqual(editForm.Username);
            actual.FirstName.ShouldEqual(editForm.FirstName);
            actual.LastName.ShouldEqual(editForm.LastName);
            actual.Title.ShouldEqual(editForm.Title);
            actual.Location.ShouldEqual(editForm.Location);
            actual.Email.ShouldEqual(editForm.Email);
            actual.PhoneNumber.ShouldEqual(editForm.PhoneNumber);

            stub.AuthenticateUsername.ShouldNotEqual(employeeToEdit.Username);
        }

        public void EditFieldsShouldNotBeEmpty()
        {
            var validator = Validator<EditEmployeeForm>();

            validator.ShouldHaveValidationErrorFor(x => x.Username, "");
            validator.ShouldHaveValidationErrorFor(x => x.FirstName, "");
            validator.ShouldHaveValidationErrorFor(x => x.LastName, "");
            validator.ShouldHaveValidationErrorFor(x => x.Title, "");
        }

        public void EditFormShouldHaveValidEmailWhenProvided()
        {
            var validator = Validator<EditEmployeeForm>();

            validator.ShouldNotHaveValidationErrorFor(x => x.Email , (string) null );
            validator.ShouldNotHaveValidationErrorFor(x => x.Email , "person@example.com");
            validator.ShouldHaveValidationErrorFor(x => x.Email , "person at notavalidemail");
        }

        public void ShouldNotAllowEditToExistingUsername(Employee initialEmployee)
        {
            var existingUsername = initialEmployee.Username;
            Save(initialEmployee);

            var validator = Validator<EditEmployeeForm>();

            validator.ShouldHaveValidationErrorFor(x => x.Username, existingUsername);
        }
    }
}
