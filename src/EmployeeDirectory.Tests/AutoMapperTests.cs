﻿using AutoMapper;

namespace EmployeeDirectory.Tests
{
    public class AutoMapperTests
    {
        public void ShouldHaveValidConfiguration()
        {
            Mapper.AssertConfigurationIsValid();
        }
    }
}