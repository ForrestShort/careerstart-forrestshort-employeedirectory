﻿using EmployeeDirectory.Core;

namespace EmployeeDirectory.Tests
{
    public class EmployeeMapTests : MapTests<Employee>
    {
    }

    public class RoleMapTests : MapTests<Role>
    {
    }

    public class RolePermissionMapTests : MapTests<RolePermission>
    {
    }

    public class EmployeeRoleMapTests : MapTests<EmployeeRole>
    {
    }
}

