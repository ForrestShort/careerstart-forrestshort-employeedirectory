﻿using System;
using System.Linq;
using EmployeeDirectory.Core;
using EmployeeDirectory.Infrastructure;
using FluentValidation;
using MediatR;

namespace EmployeeDirectory.Tests
{
    /// <summary>
    /// Provides shorthand for common test operations.
    /// </summary>
    public abstract class Tests
    {
        protected static void LogSql()
        {
            Resolve<DirectoryContext>().Database.Log = Console.Write;
        }

        private static T Resolve<T>()
        {
            return TestDependencyScope.Resolve<T>();
        }

        private static object Resolve(Type type)
        {
            return TestDependencyScope.Resolve(type);
        }

        protected TResult Query<TResult>(Func<DirectoryContext, TResult> query)
        {
            TResult result = default(TResult);
            Txn(dbContext => {result = query(dbContext); });
            return result;
        }


        protected void Save(params object[] entities)
        {
            Txn(dbContext =>
            {
                foreach (var entity in entities)
                {
                    var entry = dbContext.ChangeTracker
                        .Entries()
                        .FirstOrDefault(entityEntry => entityEntry.Entity == entity);

                    if (entry == null)
                    {
                        dbContext.Set(entity.GetType()).Add(entity);
                    }
                }
            });
        }

        protected void Send(IRequest message)
        {
            Send((IRequest<Unit>) message);
        }

        protected TResult Send<TResult>(IRequest<TResult> message)
        {
            var dbContext = Resolve<DirectoryContext>();
            var mediator = Resolve<IMediator>();
            TResult result;

            try
            {
                dbContext.BeginTransaction();
                result = mediator.Send(message);
                dbContext.CloseTransaction();
            }
            catch (Exception ex)
            {
                dbContext.CloseTransaction(ex);
                throw;
            }

            return result;
        }

        protected void Txn(Action<DirectoryContext> action)
        {
            var dbContext = new DirectoryContext();

            try
            {
                dbContext.BeginTransaction();
                action(dbContext);
                dbContext.CloseTransaction();
            }
            catch (Exception ex)
            {
                dbContext.CloseTransaction(ex);
                throw;
            }
        }

        protected void Reload<TEntity, TIdentity>(ref TEntity entity, TIdentity id) where TEntity : class
        {
            TEntity e = entity;

            Txn(dbContext => e = dbContext.Set<TEntity>().Find(id));

            entity = e;
        }

        protected TEntity Load<TEntity>(object id) where TEntity : Entity
        {
            TEntity e = null;

            Txn(dbContext => e = dbContext.Set<TEntity>().Find(id));

            return e;
        }

        protected IValidator<T> Validator<T>()
        {
            var validatorType = typeof(IValidator<>).MakeGenericType(typeof(T));
            return Resolve(validatorType) as IValidator<T>;
        }

        protected void LoginAs(Employee employee)
        {
            Resolve<RequestContext>().User = Resolve<DirectoryContext>().Employees.Single(x => x.Id == employee.Id);
        }

        public class StubAuthentication : IAuthenticationService
        {
            public string AuthenticateUsername { get; set; }

            public void SetAuthCookie(string username)
            {
                AuthenticateUsername = username;
            }

            public void SignOut()
            {
                AuthenticateUsername = null;
            }
        }

        protected void Inject<T>(T item) where T : class
        {
            TestDependencyScope.Inject(item);
        }
    }
}