﻿using System.Linq;
using EmployeeDirectory.Core;
using EmployeeDirectory.Features.Employee;
using EmployeeDirectory.Infrastructure;
using FluentValidation.TestHelper;
using Should;


namespace EmployeeDirectory.Tests
{
    public class EditProfileTests : Tests
    {
        public void TestProfileQueryHandler(Employee employee, EditProfileQuery query)
        {
            Save(employee);
            LoginAs(employee);
            var form = Send(query);

            form.Username.ShouldEqual(employee.Username);
            form.FirstName.ShouldEqual(employee.FirstName);
            form.LastName.ShouldEqual(employee.LastName);
            form.Title.ShouldEqual(employee.Title);
            form.Location.ShouldEqual(employee.Location);
            form.Email.ShouldEqual(employee.Email);
            form.PhoneNumber.ShouldEqual(employee.PhoneNumber);

            

        }
        public void EditMyProfile(Employee myself, EditProfileForm editForm)
        {
            Save(myself);
            LoginAs(myself);
            var stub = new StubAuthentication();
            Inject<IAuthenticationService>(stub);

            Send(editForm);
            Query(x => x.Employees).ToList().Count.ShouldEqual(1);

            var actual = Query(x => x.Employees).Single(x => x.Id == myself.Id);

            actual.Username.ShouldEqual(editForm.Username);
            actual.FirstName.ShouldEqual(editForm.FirstName);
            actual.LastName.ShouldEqual(editForm.LastName);
            actual.Title.ShouldEqual(editForm.Title);
            actual.Location.ShouldEqual(editForm.Location);
            actual.Email.ShouldEqual(editForm.Email);
            actual.PhoneNumber.ShouldEqual(editForm.PhoneNumber);

            stub.AuthenticateUsername.ShouldEqual(editForm.Username);
        }

        public void EditFieldsShouldNotBeEmpty()
        {
            var validator = Validator<EditProfileForm>();

            validator.ShouldHaveValidationErrorFor(x => x.Username, "");
            validator.ShouldHaveValidationErrorFor(x => x.FirstName, "");
            validator.ShouldHaveValidationErrorFor(x => x.LastName, "");
            validator.ShouldHaveValidationErrorFor(x => x.Title, "");
        }

        public void EditFormShouldHaveValidEmailWhenProvided()
        {
            var validator = Validator<EditEmployeeForm>();

            validator.ShouldNotHaveValidationErrorFor(x => x.Email, (string)null);
            validator.ShouldNotHaveValidationErrorFor(x => x.Email, "person@example.com");
            validator.ShouldHaveValidationErrorFor(x => x.Email, "person at notavalidemail");
        }

        public void ShouldNotAllowEditingToExistingUsername(Employee initialEmployee)
        {
            var existingUsername = initialEmployee.Username;
            Save(initialEmployee);

            var validator = Validator<EditProfileForm>();

            validator.ShouldHaveValidationErrorFor(x => x.Username, existingUsername);
        }
    }
}