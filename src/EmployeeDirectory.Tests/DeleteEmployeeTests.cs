﻿using System.Linq;
using EmployeeDirectory.Core;
using EmployeeDirectory.Features.Employee;
using FluentValidation.TestHelper;
using Should;

namespace EmployeeDirectory.Tests
{
    public class DeleteEmployeeTests : Tests
    {
        public void Delete(Employee employeeToDelete, Employee employeeToKeep, DeleteEmployeeCommand deleteCommand)
        {
            Save(employeeToDelete, employeeToKeep);

            deleteCommand.Id = employeeToDelete.Id;

            Send(deleteCommand);

            Query(x => x.Employees).ToList().Count.ShouldEqual(1);

            var actual = Query(x => x.Employees).Single();
            actual.FirstName.ShouldEqual(employeeToKeep.FirstName);
            actual.LastName.ShouldEqual(employeeToKeep.LastName);
            actual.Title.ShouldEqual(employeeToKeep.Title);
            actual.Location.ShouldEqual(employeeToKeep.Location);
            actual.Email.ShouldEqual(employeeToKeep.Email);
            actual.PhoneNumber.ShouldEqual(employeeToKeep.PhoneNumber);
        }

        public void DeleteMyself(Employee myself)
        {
            Save(myself);
            LoginAs(myself);
            
            var validator = Validator<DeleteEmployeeCommand>();

            validator.ShouldHaveValidationErrorFor(x => x.Id, myself.Id);
        }
    }
}

