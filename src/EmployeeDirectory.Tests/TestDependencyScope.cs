﻿using System;
using EmployeeDirectory.DependencyResolution;
using StructureMap;

namespace EmployeeDirectory.Tests
{
    public static class TestDependencyScope
    {
        private static IContainer _currentNestedContainer;

        public static void Begin()
        {
            if (_currentNestedContainer != null)
                throw new Exception("Cannot begin test dependency scope. Another dependency scope is still in effect.");

            _currentNestedContainer = IoC.Container.GetNestedContainer();
        }

        public static T Resolve<T>()
        {
            if (_currentNestedContainer == null)
                throw new Exception(string.Format("Cannot resolve type {0}. There is no test dependency scope in effect.", typeof(T)));

            return _currentNestedContainer.GetInstance<T>();
        }

        public static object Resolve(Type type)
        {
            if (_currentNestedContainer == null)
                throw new Exception(string.Format("Cannot resolve type {0}. There is no test dependency scope in effect.", type));

            return _currentNestedContainer.GetInstance(type);
        }

        public static void End()
        {
            if (_currentNestedContainer == null)
                throw new Exception("Cannot end test dependency scope. There is no current dependency scope to end.");

            _currentNestedContainer.Dispose();
            _currentNestedContainer = null;
        }

        public static void Inject<T>(T instance) where T : class
        {
            if (_currentNestedContainer == null)
                throw new Exception(
                    string.Format("Cannot inject instance for type {0}. There is no test dependency scope in effect.",
                        typeof (T)));

            _currentNestedContainer.Inject(instance);
        }
    }
}