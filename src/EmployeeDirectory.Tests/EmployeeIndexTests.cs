﻿using EmployeeDirectory.Core;
using EmployeeDirectory.Features.Employee;
using Should;

namespace EmployeeDirectory.Tests
{
    public class EmployeeIndexTests : Tests
    {
        public void ShouldGiveSortedEmployees(Employee firstEmployee, Employee secondEmployee, Employee thirdEmployee)
        {
            firstEmployee.FirstName = "AC";
            firstEmployee.LastName = "AA";

            secondEmployee.FirstName = "AB";
            secondEmployee.LastName = "AA";

            thirdEmployee.FirstName = "AA";
            thirdEmployee.LastName = "CA";

            Save(firstEmployee, secondEmployee, thirdEmployee);
            var sortedEmployeeArray = Send(new EmployeeIndexQuery());

            sortedEmployeeArray[0].FirstName.ShouldEqual("AB");
            sortedEmployeeArray[0].LastName.ShouldEqual("AA");

            sortedEmployeeArray[1].FirstName.ShouldEqual("AC");
            sortedEmployeeArray[1].LastName.ShouldEqual("AA");

            sortedEmployeeArray[2].FirstName.ShouldEqual("AA");
            sortedEmployeeArray[2].LastName.ShouldEqual("CA");
        }
    }
}
