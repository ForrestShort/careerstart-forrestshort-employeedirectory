﻿using EmployeeDirectory.Core;

namespace EmployeeDirectory.Tests
{
    public abstract class MapTests<TEntity> : Tests where TEntity : Entity
    {
        public void ShouldPersistAllProperties(TEntity entity)
        {
            Save(entity);

            var result = Load<TEntity>(entity.Id);

            result.ShouldBe(entity);
        }
    }
}