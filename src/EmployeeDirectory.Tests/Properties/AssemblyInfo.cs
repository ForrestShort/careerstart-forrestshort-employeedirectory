﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: ComVisible(false)]
[assembly: AssemblyProduct("EmployeeDirectory")]
[assembly: AssemblyTitle("EmployeeDirectory.Tests")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyCopyright("Copyright © 2015 ")]
[assembly: AssemblyCompany("Headspring")]
[assembly: AssemblyConfiguration("Release")]