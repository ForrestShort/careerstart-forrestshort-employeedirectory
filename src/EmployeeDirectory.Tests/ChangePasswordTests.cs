﻿using EmployeeDirectory.Core;
using EmployeeDirectory.Features.Account;
using EmployeeDirectory.Infrastructure;
using Should;

namespace EmployeeDirectory.Tests
{
    public class ChangePasswordTests : Tests
    {
        public void ChangePassword(Employee employeePasswordToChange)
        {
            employeePasswordToChange.Password = PasswordService.HashPassword("password123");
            Save(employeePasswordToChange);
            LoginAs(employeePasswordToChange);

            var badPasswordForm = new ChangePasswordForm
            {
                CurrentPassword = "Not The Password",
                NewPassword = "321",
                ConfirmPassword = "123"
            };

            var anotherBadPasswordForm = new ChangePasswordForm
            {
                CurrentPassword = employeePasswordToChange.Password,
                NewPassword = "321",
                ConfirmPassword = "123"
            };

            var goodPasswordForm = new ChangePasswordForm
            {
                CurrentPassword = "password123" ,
                NewPassword = "123",
                ConfirmPassword = "123"
            };

            var validator = Validator<ChangePasswordForm>();

            validator.Validate(badPasswordForm).IsValid.ShouldBeFalse();
            validator.Validate(anotherBadPasswordForm).IsValid.ShouldBeFalse();
            validator.Validate(goodPasswordForm).IsValid.ShouldBeTrue();
        } 
    }
}