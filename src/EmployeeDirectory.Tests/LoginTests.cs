﻿using EmployeeDirectory.Core;
using EmployeeDirectory.Features.Account;
using EmployeeDirectory.Infrastructure;
using Should;

namespace EmployeeDirectory.Tests
{
    public class LoginTests : Tests
    {
        public void Login(Employee employeeInSystem, Employee anotherEmployeeInSystem)
        {
            employeeInSystem.Password = PasswordService.HashPassword("password123");
            Save(employeeInSystem, anotherEmployeeInSystem);

            var username = employeeInSystem.Username;

            var badLoginForm = new LoginForm
            {
                Username = "Not The Username",
                Password = "Not The Password"

            };

            var anotherBadLoginForm = new LoginForm
            {
                Username = "Not The Username",
                Password = "password123"

            };

            var lastBadLoginForm = new LoginForm
            {
                Username = username,
                Password = "Not The Password"

            };

            var goodLoginForm = new LoginForm
            {
                Username = username,
                Password = "password123"

            };

            var validator = Validator<LoginForm>();

            validator.Validate(badLoginForm).IsValid.ShouldBeFalse();
            validator.Validate(anotherBadLoginForm).IsValid.ShouldBeFalse();
            validator.Validate(lastBadLoginForm).IsValid.ShouldBeFalse();
            validator.Validate(goodLoginForm).IsValid.ShouldBeTrue();
        }

        public void ShouldAuthenticateCorrectly(Employee employee, LoginForm loginForm)
        {
            employee.Username = "Administrator";
            employee.Password = "pasword123";
            
            Save(employee);

            loginForm.Username = "Administrator";
            loginForm.Password = "password123";

            var stub = new StubAuthentication();
            Inject<IAuthenticationService>(stub);

            Send(loginForm);

            stub.AuthenticateUsername.ShouldEqual("Administrator");
        }
    }
}