﻿using System.Linq;
using EmployeeDirectory.Core;
using EmployeeDirectory.Features.Employee;
using EmployeeDirectory.Infrastructure;
using FluentValidation.TestHelper;
using Should;

namespace EmployeeDirectory.Tests
{
    public class AddEmployeeTests : Tests
    {
        public void Add(Employee initialEmployee, CreateEmployeeForm createCommand)
        {
            var nonHashedAddedEmployeePassword = createCommand.Password;
            Save(initialEmployee);

            var result = Query(x => x.Employees).ToList();
            result.Count().ShouldEqual(1);

            Send(createCommand);

            result = Query(x => x.Employees).ToList();
            result.Count().ShouldEqual(2);

            var addedEmployee = result.Single(x => x.Username != initialEmployee.Username);
            addedEmployee.FirstName.ShouldEqual(createCommand.FirstName);
            addedEmployee.LastName.ShouldEqual(createCommand.LastName);
            addedEmployee.Title.ShouldEqual(createCommand.Title);
            addedEmployee.Location.ShouldEqual(createCommand.Location);
            addedEmployee.Email.ShouldEqual(createCommand.Email);
            addedEmployee.PhoneNumber.ShouldEqual(createCommand.PhoneNumber);
            addedEmployee.Username.ShouldEqual(createCommand.Username);

            PasswordService.Verify(nonHashedAddedEmployeePassword, addedEmployee.Password).ShouldBeTrue();
        }

        public void AddFieldsShouldNotBeEmpty()
        {
            var validator = Validator <CreateEmployeeForm>();

            validator.ShouldHaveValidationErrorFor(x => x.FirstName, "");
            validator.ShouldHaveValidationErrorFor(x => x.LastName, "");
            validator.ShouldHaveValidationErrorFor(x => x.Title, "");
            validator.ShouldHaveValidationErrorFor(x => x.Username, "");
            validator.ShouldHaveValidationErrorFor(x => x.Password, "");
            validator.ShouldHaveValidationErrorFor(x => x.ConfirmPassword, "");
        }

        public void AddFormShouldHaveValidEmailWhenProvided()
        {
            var validator = Validator<CreateEmployeeForm>();

            validator.ShouldNotHaveValidationErrorFor(x => x.Email, (string)null);
            validator.ShouldNotHaveValidationErrorFor(x => x.Email, "person@example.com");
            validator.ShouldHaveValidationErrorFor(x => x.Email, "person at notavalidemail");
        }

        public void ShouldNotAllowSameDuplicateUsername(Employee initialEmployee, CreateEmployeeForm employeeToTryAndAdd)
        {
            var initialEmployeeUsername = initialEmployee.Username;
            Save(initialEmployee);

            var validator = Validator <CreateEmployeeForm>();
            validator.ShouldHaveValidationErrorFor(x => x.Username, initialEmployeeUsername);
        }
    }
}
