﻿using Newtonsoft.Json;
using Should;

namespace EmployeeDirectory.Tests
{
    public static class Assertions
    {
        public static void ShouldBe<T>(this T actual, T expected)
        {
            Json(actual).ShouldEqual(Json(expected), "Expected two objects to match on all properties.");
        }

        private static string Json<T>(T actual)
        {
            return JsonConvert.SerializeObject(actual, Formatting.Indented);
        }
    }
}