﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace EmployeeDirectory.Core
{
    public class DirectoryContext : DbContext
    {
        private DbContextTransaction _currentTransaction;

        private static readonly string ConnectionString =
            ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

        public DirectoryContext()
            : base(ConnectionString)
        {
            
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<RolePermission> Permissions { get; set; }
        public DbSet<EmployeeRole> EmployeesRoles { get; set; }

        public void BeginTransaction()
        {
            if (_currentTransaction != null)
                return;

            _currentTransaction = Database.BeginTransaction(IsolationLevel.ReadCommitted);
        }

        public void CloseTransaction()
        {
            CloseTransaction(exception: null);
        }

        public void CloseTransaction(Exception exception)
        {
            try
            {
                if (_currentTransaction != null && exception != null)
                {
                    _currentTransaction.Rollback();
                    return;
                }

                SaveChanges();

                if (_currentTransaction != null)

                    _currentTransaction.Commit();
            }
            catch (Exception)
            {
                if (_currentTransaction != null && _currentTransaction.UnderlyingTransaction.Connection != null)
                    _currentTransaction.Rollback();

                throw;
            }
            finally
            {
                if (_currentTransaction != null)
                {
                    _currentTransaction.Dispose();
                    _currentTransaction = null;
                }
            }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Add<IdentityConvention>();
            modelBuilder.Conventions.Add<ForeignKeyNamingConvention>();
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Configurations.AddFromAssembly(typeof(DirectoryContext).Assembly);

        }

        public class IdentityConvention : Convention
        {
            private const string Id = "Id";

            public IdentityConvention()
            {
                Properties<Guid>()
                    .Where(p => p.Name == Id)
                    .Configure(p => p
                        .IsKey()
                        .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity));
            }
        }

        public class ForeignKeyNamingConvention : IStoreModelConvention<AssociationType>
        {
            public void Apply(AssociationType association, DbModel model)
            {
                if (association.IsForeignKey)
                {
                    var constraint = association.Constraint;

                    NormalizeForeignKeyProperties(constraint.FromProperties);
                    NormalizeForeignKeyProperties(constraint.ToProperties);
                }
            }

            private static void NormalizeForeignKeyProperties(IEnumerable<EdmProperty> properties)
            {
                foreach (var property in properties)
                {
                    int underscoreIndex = property.Name.IndexOf('_');

                    if (underscoreIndex > 0)
                        property.Name = property.Name.Remove(underscoreIndex, 1);
                }
            }
        }
    }
}