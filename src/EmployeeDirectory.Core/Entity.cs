﻿using System;

namespace EmployeeDirectory.Core
{
    public abstract class Entity
    {
        public Guid Id { get; set; }
    }
}