﻿DECLARE @HRID UNIQUEIDENTIFIER;
SELECT @HRID = [Id] FROM [dbo].[Role] WHERE [RoleName] = 'Human Resources';
DECLARE @MANAGERID UNIQUEIDENTIFIER;
SELECT @MANAGERID = [Id] FROM [dbo].[Role] WHERE [RoleName] = 'Manager';
DECLARE @SYSTEMADMINID UNIQUEIDENTIFIER;
SELECT @SYSTEMADMINID = [Id] FROM [dbo].[Role] WHERE [RoleName] = 'System Administrator';

DECLARE @ADMINUSER UNIQUEIDENTIFIER;
SELECT @ADMINUSER = Id FROM [dbo].[Employee] WHERE [Username] = 'Admin';



INSERT INTO [dbo].[RolePermission] ([RoleId], [Permission]) VALUES (
@HRID,
'1'
);
INSERT INTO [dbo].[RolePermission] ([RoleId], [Permission]) VALUES (
@HRID,
'2'
);
INSERT INTO [dbo].[RolePermission] ([RoleId], [Permission]) VALUES (
@HRID,
'3'
);

INSERT INTO [dbo].[RolePermission] ([RoleId], [Permission]) VALUES (
@MANAGERID,
'3'
);

INSERT INTO [dbo].[RolePermission] ([RoleId], [Permission]) VALUES (
@SYSTEMADMINID,
'1'
);
INSERT INTO [dbo].[RolePermission] ([RoleId], [Permission]) VALUES (
@SYSTEMADMINID,
'2'
);
INSERT INTO [dbo].[RolePermission] ([RoleId], [Permission]) VALUES (
@SYSTEMADMINID,
'3'
);
INSERT INTO [dbo].[RolePermission] ([RoleId], [Permission]) VALUES (
@SYSTEMADMINID,
'4'
);

INSERT INTO [dbo].[EmployeeRole] ([EmployeeId], [RoleId]) VALUES (
@ADMINUSER,
@SYSTEMADMINID
);