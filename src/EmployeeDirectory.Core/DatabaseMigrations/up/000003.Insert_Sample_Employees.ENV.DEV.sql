﻿INSERT INTO [dbo].[Employee] ([Username], [Password], [FirstName], [LastName], [Title], [Location], [Email], [PhoneNumber]) VALUES (
'JT1',
'$2a$10$vI4mSa3IodPMwmkpglke6eY6FgDuR5l.kapUx061H9KPN2soZ2lOi',
'JT',
'McCormick',
'President',
0, --Austin
'JT@example.com',
'555-873-4501'
);

INSERT INTO [dbo].[Employee] ([Username],[Password], [FirstName], [LastName], [Title], [Location], [Email], [PhoneNumber]) VALUES (
'Forrest1',
'$2a$10$vI4mSa3IodPMwmkpglke6eY6FgDuR5l.kapUx061H9KPN2soZ2lOi',
'Forrest',
'Short',
'Intern',
1, --Dallas
'forrest@example.com',
'316-987-4567'
);

INSERT INTO [dbo].[Employee] ([Username], [Password], [FirstName], [LastName], [Title], [Location], [Email], [PhoneNumber]) VALUES (
'Dustin1',
'$2a$10$vI4mSa3IodPMwmkpglke6eY6FgDuR5l.kapUx061H9KPN2soZ2lOi',
'Dustin',
'Wells',
'CEO',
0, --Austin
'dustin@example.com',
'555-839-2949'
);

INSERT INTO [dbo].[Employee] ([Username], [Password], [FirstName], [LastName], [Title], [Location], [Email], [PhoneNumber]) VALUES (
'Sharon1',
'$2a$10$vI4mSa3IodPMwmkpglke6eY6FgDuR5l.kapUx061H9KPN2soZ2lOi',
'Sharon',
'Cichelli',
'Principal Consultant',
0, --Austin
'sharon@example.com',
'555-186-2829'
);