﻿using System.Data.Entity;

namespace EmployeeDirectory.Core.DatabaseMigrations
{
    public class DirectoryDbConfiguration : DbConfiguration
    {
        public DirectoryDbConfiguration()
        {
            SetDatabaseInitializer<DirectoryContext>(null);
        }
    }
}