﻿namespace EmployeeDirectory.Core
{
    public enum Permission
    {
        RegisterEmployees = 1,
        DeleteEmployees = 2,
        EditEmployees = 3,
        ManageSecurity = 4
    }

}