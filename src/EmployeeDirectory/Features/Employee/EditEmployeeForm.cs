﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using EmployeeDirectory.Core;
using FluentValidation;
using FluentValidation.Results;
using MediatR;

namespace EmployeeDirectory.Features.Employee
{
    public class EditEmployeeForm : IRequest
    {
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Title")]
        public string Title { get; set; }

        [Display(Name = "Location")]
        public Location Location { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Username")]
        public string Username { get; set; }

        public Guid Id { get; set; }
    }

    public class EditEmployeeValidator : AbstractValidator<EditEmployeeForm>
    {
        private readonly DirectoryContext _context;

        public EditEmployeeValidator(DirectoryContext directoryContext)
        {
            _context = directoryContext;

            RuleFor(form => form.Username).NotEmpty().Length(1, 100);
            RuleFor(form => form.FirstName).NotEmpty().Length(1, 100);
            RuleFor(form => form.LastName).NotEmpty().Length(1, 100);
            RuleFor(form => form.Title).NotEmpty().Length(1, 100);
            RuleFor(form => form.Email).Length(0, 100);
            RuleFor(form => form.PhoneNumber).Length(0, 100);

            RuleFor(form => form.Location).NotNull();

            RuleFor(form => form.Email).EmailAddress();

            Custom(form =>
            {
                var employeeToCheck = _context.Employees.SingleOrDefault(x => x.Username == form.Username);
                if (employeeToCheck != null && employeeToCheck.Id == form.Id)
                {
                    return null;
                }
                return employeeToCheck == null ? null : new ValidationFailure("Username", "Username has already been used");
            });
        }
    }
} 
