﻿using EmployeeDirectory.Features.Account;

namespace EmployeeDirectory.Features.Employee
{
using AutoMapper;
    
    public class MappingProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<Core.Employee, EmployeeViewModel>();
            CreateMap<EditEmployeeForm, Core.Employee>().ForMember(x => x.Password, opt => opt.Ignore());
            CreateMap<CreateEmployeeForm, Core.Employee>().ForMember(x => x.Id, opt => opt.Ignore());
            CreateMap<Core.Employee, EditEmployeeForm>();
            CreateMap<Core.Employee, EditProfileForm>();
            CreateMap<EditProfileForm, Core.Employee>().ForMember(x => x.Password, opt => opt.Ignore()).ForMember(x => x.Id, opt => opt.Ignore());
        }
    }
}