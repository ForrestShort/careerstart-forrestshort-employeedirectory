﻿using System;
using MediatR;

namespace EmployeeDirectory.Features.Employee
{
    public class SetRolesForm : IRequest
    {
        public Guid Id { get; set; }
        public string EmployeeName { get; set; }
        public  RolesSelection[] Roles{ get; set; } 
    }

    public class RolesSelection
    {
        public Guid RoleId { get; set; }
        public string Name { get; set; }
        public bool Selected { get; set; }
    }
}