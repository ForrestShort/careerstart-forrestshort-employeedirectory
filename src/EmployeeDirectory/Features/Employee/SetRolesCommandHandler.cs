﻿using System.Data.SqlClient;
using EmployeeDirectory.Core;
using MediatR;

namespace EmployeeDirectory.Features.Employee
{
    public class SetRolesCommandHandler : RequestHandler<SetRolesForm>
    {
        private readonly DirectoryContext _context;

        public SetRolesCommandHandler(DirectoryContext context)
        {
            _context = context;
        }
        protected override void HandleCore(SetRolesForm form)
        {
            var employee = _context.Employees.Find(form.Id);
            _context.Database.ExecuteSqlCommand(
                @"DELETE FROM [EmployeeRole] WHERE [EmployeeId] = @EmployeeId",
                new SqlParameter("EmployeeId", employee.Id));

            for (int i = 0; i < form.Roles.Length; i++)
            {
                if (form.Roles[i].Selected)
                {
                    _context.Database.ExecuteSqlCommand(
                    @"INSERT INTO [EmployeeRole]([EmployeeId], [RoleId]) VALUES (@EmployeeId, @RoleId)",
                new SqlParameter("EmployeeId", employee.Id), new SqlParameter ("RoleId", form.Roles[i].RoleId));
                }
                
            }
        }
    }
   
}