﻿using AutoMapper;
using EmployeeDirectory.Core;
using EmployeeDirectory.Infrastructure;
using MediatR;

namespace EmployeeDirectory.Features.Employee
{
    public class CreateEmployeeCommandHandler : RequestHandler<CreateEmployeeForm>
    {
        private readonly DirectoryContext _context;

        public CreateEmployeeCommandHandler(DirectoryContext context)
        {
            _context = context;
        }

        protected override void HandleCore(CreateEmployeeForm form)
        {
            {
                form.Password = PasswordService.HashPassword(form.Password);
                var mapped = Mapper.Map<CreateEmployeeForm, Core.Employee>(form);
                _context.Employees.Add(mapped);
            }
        }
    }
}