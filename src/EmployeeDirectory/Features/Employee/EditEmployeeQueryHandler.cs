﻿using System;
using System.Linq;
using AutoMapper;
using EmployeeDirectory.Core;
using MediatR;

namespace EmployeeDirectory.Features.Employee
{
    public class EditEmployeeQuery : IRequest<EditEmployeeForm>
    {
        public Guid Id { get; set; }
    }

    public class EditEmployeeQueryHandler : IRequestHandler<EditEmployeeQuery, EditEmployeeForm>
    {
        private readonly DirectoryContext _context;

        public EditEmployeeQueryHandler(DirectoryContext context)
        {
            _context = context;
        }

        public EditEmployeeForm Handle(EditEmployeeQuery query)
        {
            var employee = _context.Employees.Single(x => x.Id == query.Id);
            var mapped = Mapper.Map<Core.Employee, EditEmployeeForm>(employee);
            return mapped;
        }
    }
}