﻿using System;
using System.Data.SqlClient;
using EmployeeDirectory.Core;
using EmployeeDirectory.Infrastructure;
using FluentValidation;
using MediatR;

namespace EmployeeDirectory.Features.Employee
{
    public class DeleteEmployeeCommand : IRequest
    {
        public Guid Id { get; set; }
    }

    public class DeleteValidator : AbstractValidator<DeleteEmployeeCommand>
    {
        public DeleteValidator(RequestContext context)
        {
            RuleFor(form => form.Id).NotEqual(context.User.Id);
        }
    }

    public class DeleteEmployeeCommandHandler : RequestHandler<DeleteEmployeeCommand>
    {
        private readonly DirectoryContext _context;

        public DeleteEmployeeCommandHandler(DirectoryContext context)
        {
            _context = context;
        }

        protected override void HandleCore(DeleteEmployeeCommand message)
        {
            _context.Database.ExecuteSqlCommand(
                @"DELETE FROM [EmployeeRole] WHERE [EmployeeId] = @EmployeeId",
                new SqlParameter("EmployeeId", message.Id));

                _context.Employees.Remove(_context.Employees.Find(message.Id));
        }
    }
}