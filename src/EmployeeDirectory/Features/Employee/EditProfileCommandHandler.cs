﻿using AutoMapper;
using EmployeeDirectory.Infrastructure;
using MediatR;

namespace EmployeeDirectory.Features.Employee
{
    public class EditProfileCommandHandler : RequestHandler<EditProfileForm>
    {
        private readonly RequestContext _userContext;
        private readonly IAuthenticationService _cookie;

        public EditProfileCommandHandler(RequestContext userContext,
            IAuthenticationService cookie)
        {
            _userContext = userContext;
            _cookie = cookie;
        }

        protected override void HandleCore(EditProfileForm form)
        {
            var employee = _userContext.User;
            Mapper.Map(form, employee);
            _userContext.User = employee;
            _cookie.SignOut();
            _cookie.SetAuthCookie(employee.Username);
        }
    }
}
