﻿using AutoMapper;
using EmployeeDirectory.Infrastructure;
using MediatR;

namespace EmployeeDirectory.Features.Employee
{
    public class EditProfileQuery : IRequest<EditProfileForm>{}

    public class EditProfileQueryHandler : IRequestHandler<EditProfileQuery, EditProfileForm>
    {
        private readonly RequestContext _context;

        public EditProfileQueryHandler(RequestContext context)
        {
            _context = context;
        }

        public EditProfileForm Handle(EditProfileQuery query)
        {
            var employee = _context.User;
            var mapped = Mapper.Map<Core.Employee, EditProfileForm>(employee);
            return mapped;
        }
    }
}