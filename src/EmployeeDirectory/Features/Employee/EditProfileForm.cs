﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using EmployeeDirectory.Core;
using EmployeeDirectory.Infrastructure;
using FluentValidation;
using FluentValidation.Results;
using MediatR;

namespace EmployeeDirectory.Features.Employee
{
    public class EditProfileForm : IRequest
    {
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Title")]
        public string Title { get; set; }

        [Display(Name = "Location")]
        public Location Location { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Username")]
        public string Username { get; set; }

    }

    public class EditProfileValidator : AbstractValidator<EditProfileForm>
    {
        private readonly DirectoryContext _directoryContext;
        private readonly RequestContext _userContext;

        public EditProfileValidator(DirectoryContext directoryContext, RequestContext userContext)
        {
            _directoryContext = directoryContext;
            _userContext = userContext;

            RuleFor(form => form.Username).NotEmpty().Length(1, 100);
            RuleFor(form => form.FirstName).NotEmpty().Length(1, 100);
            RuleFor(form => form.LastName).NotEmpty().Length(1, 100);
            RuleFor(form => form.Title).NotEmpty().Length(1, 100);
            RuleFor(form => form.Email).Length(0, 100);
            RuleFor(form => form.PhoneNumber).Length(0, 100);

            RuleFor(form => form.Location).NotNull();

            RuleFor(form => form.Email).EmailAddress();

            Custom(form =>
            {
                var employeeToCheck = _directoryContext.Employees.SingleOrDefault(x => x.Username == form.Username);
                if (employeeToCheck != null && employeeToCheck == _userContext.User)
                {
                    return null;
                }
                return employeeToCheck == null ? null : new ValidationFailure("Username", "Username has already been used");
            });
        }
    }
}