﻿using System.Linq;
using AutoMapper;
using EmployeeDirectory.Core;
using MediatR;

namespace EmployeeDirectory.Features.Employee
{
    public class EmployeeIndexQuery : IRequest<EmployeeViewModel[]>
    {
    }

    public class EmployeeIndexQueryHandler : IRequestHandler<EmployeeIndexQuery, EmployeeViewModel[]>
    {
        private readonly DirectoryContext _context;

        public EmployeeIndexQueryHandler(DirectoryContext context)
        {
            _context = context;
        }

        public EmployeeViewModel[] Handle(EmployeeIndexQuery message)
        {
            var model = _context.Employees
                .OrderBy(x => x.LastName)
                .ThenBy(x => x.FirstName)
                .Select(Mapper.Map<Core.Employee, EmployeeViewModel>)
                .ToArray();
            return model;
        }
    }
}