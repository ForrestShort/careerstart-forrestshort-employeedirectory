﻿using System.Linq;
using AutoMapper;
using EmployeeDirectory.Core;
using EmployeeDirectory.Infrastructure;
using MediatR;

namespace EmployeeDirectory.Features.Employee
{
    public class EditEmployeeCommandHandler : RequestHandler<EditEmployeeForm>
    {
        private readonly DirectoryContext _context;
        private readonly RequestContext _userContext;
        private readonly IAuthenticationService _cookie;

        public EditEmployeeCommandHandler(DirectoryContext context, RequestContext userContext, IAuthenticationService cookie)
        {
            _context = context;
            _userContext = userContext;
            _cookie = cookie;
        }

        protected override void HandleCore(EditEmployeeForm form)
        {
            var employee = _context.Employees.Single(x => x.Id == form.Id);
            Mapper.Map(form, employee);
            if (employee.Id == _userContext.User.Id)
            {
                _userContext.User = employee;
                _cookie.SignOut();
                _cookie.SetAuthCookie(employee.Username);
            }
        }
    }
}

