﻿using System;
using System.Linq;
using EmployeeDirectory.Core;
using MediatR;

namespace EmployeeDirectory.Features.Employee
{
    public class SetRolesQuery : IRequest<SetRolesForm>
    {
        public Guid Id { get; set; }
    }
    public class SetRolesQueryHandler : IRequestHandler<SetRolesQuery, SetRolesForm>
    {
        private readonly DirectoryContext _context;

        public SetRolesQueryHandler(DirectoryContext context)
        {
            _context = context;
        }

        public SetRolesForm Handle(SetRolesQuery query)
        {
            var employeesRoles = _context.EmployeesRoles.Where(x => x.Employee.Id == query.Id).Select(x => x.Role.Id).ToArray();
            var allRoles = _context.Roles.ToArray();


            var roles = allRoles.OrderBy(x => x.RoleName).Select(t => new RolesSelection
            {
                Name = t.RoleName, RoleId = t.Id, Selected = employeesRoles.Contains(t.Id)
            }).ToList();

            return new SetRolesForm
            {
                Id = query.Id,
                EmployeeName = _context.Employees.Single(x => x.Id == query.Id).FirstName + " " +
                _context.Employees.Single(x => x.Id == query.Id).LastName,
                Roles = roles.ToArray()
            };

        }

    }
}