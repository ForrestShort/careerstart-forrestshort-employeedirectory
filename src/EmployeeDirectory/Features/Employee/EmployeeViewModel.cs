﻿using System;
using EmployeeDirectory.Core;

namespace EmployeeDirectory.Features.Employee
{
    public class EmployeeViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Title { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public Location Location { get; set; }
        public Guid Id { get; set; }
    }
}
