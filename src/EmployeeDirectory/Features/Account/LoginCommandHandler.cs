﻿using EmployeeDirectory.Infrastructure;
using MediatR;

namespace EmployeeDirectory.Features.Account
{
    public class LoginCommandHandler : RequestHandler<LoginForm> 
    {
        private readonly IAuthenticationService _authenticate;

        public LoginCommandHandler(IAuthenticationService authenticate)
        {
            _authenticate = authenticate;
        }

        protected override void HandleCore(LoginForm message)
        {
            _authenticate.SetAuthCookie(message.Username);
        }
    }
}
