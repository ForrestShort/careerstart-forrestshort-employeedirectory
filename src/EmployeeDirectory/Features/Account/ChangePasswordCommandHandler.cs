﻿using EmployeeDirectory.Infrastructure;
using MediatR;

namespace EmployeeDirectory.Features.Account
{
    public class ChangePasswordCommandHandler : RequestHandler<ChangePasswordForm>
    {
        private readonly RequestContext _context;

        public ChangePasswordCommandHandler(RequestContext context)
        {
            _context = context;
        }

        protected override void HandleCore(ChangePasswordForm form)
        {
            var newPassword = PasswordService.HashPassword(form.NewPassword);
            _context.User.Password = newPassword;
        }
    }
}