﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using EmployeeDirectory.Core;
using EmployeeDirectory.Infrastructure;
using FluentValidation;
using FluentValidation.Results;
using MediatR;

namespace EmployeeDirectory.Features.Account
{
    public class LoginForm : IRequest
    {
        [Display(Name = "Username")]
        public string Username { get; set; }

        [DataType (DataType.Password)]
        [AllowHtml]
        [Display(Name = "Password")]
        public string Password { get; set; }

        public Guid Id { get; set; }
    }

    public class LoginValidator : AbstractValidator<LoginForm>
    {
        public LoginValidator(DirectoryContext context)
        {
            Custom(employee =>
            {
                var employeeToCheck = context.Employees.SingleOrDefault(x => x.Username == employee.Username);
                if (employeeToCheck != null)
                {
                    return PasswordService.Verify(employee.Password, employeeToCheck.Password)
                        ? null
                        : new ValidationFailure("Secret", "Username or password is incorrect.");
                }

                return new ValidationFailure("Secret", "Username or password is incorrect.");
            });
        }
    }
}