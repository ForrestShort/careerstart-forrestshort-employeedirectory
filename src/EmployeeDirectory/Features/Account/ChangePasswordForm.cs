﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using EmployeeDirectory.Infrastructure;
using FluentValidation;
using FluentValidation.Results;
using MediatR;

namespace EmployeeDirectory.Features.Account
{
    public class ChangePasswordForm : IRequest
    {
        [DataType(DataType.Password)]
        [AllowHtml]
        [Display(Name = "Current Password")]
        public string CurrentPassword { get; set; }

        [DataType(DataType.Password)]
        [AllowHtml]
        [Display(Name = "New Password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [AllowHtml]
        [Display(Name = "Confirm Password")]
        public string ConfirmPassword { get; set; }

        public Guid Id { get; set; }
    }

    public class ChangePasswordValidator : AbstractValidator<ChangePasswordForm>
    {
        public ChangePasswordValidator(RequestContext context)
        {
            RuleFor(employee => employee.CurrentPassword).NotEmpty().Length(1, 100);
            RuleFor(employee => employee.NewPassword).NotEmpty().Length(1, 100);
            RuleFor(employee => employee.ConfirmPassword).NotEmpty().Length(1, 100);

            RuleFor(x => x.ConfirmPassword).Equal(x => x.NewPassword).WithMessage("Passwords do not match.");

            Custom(employee =>
            {
                var employeeToCheck = context.User;
                    return PasswordService.Verify(employee.CurrentPassword, employeeToCheck.Password)
                        ? null
                        : new ValidationFailure("CurrentPassword", "Current password is incorrect");
            });
        }
    }
}