﻿using System;
using System.Linq;
using System.Web.Mvc;
using EmployeeDirectory.Core;

namespace EmployeeDirectory.Infrastructure
{
    public class RequirePermissions : FilterAttribute, IAuthorizationFilter
    {
        protected readonly Permission RequiredPermission;

        public RequirePermissions(Permission requiredPermission)
        {
            RequiredPermission = requiredPermission;
        }

        public void OnAuthorization(AuthorizationContext filterContext)
        {
            IsUserAuthorized();
        }
        
        public void IsUserAuthorized()
        {
            var usersPermissions = HasPermissionHelpers.GetUsersPermissions();
            if (usersPermissions.Contains(RequiredPermission))
            {
                return;
            }
            throw new UnauthorizedAccessException();
        }
    }
}