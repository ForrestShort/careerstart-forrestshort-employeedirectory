﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace EmployeeDirectory.Infrastructure
{
    public static class HtmlHelperExtensions
    {
        public static MvcHtmlString Input<TModel, TValue>(this HtmlHelper<TModel> html,
            Expression<Func<TModel, TValue>> expression)
        {
            MvcHtmlString label = html.LabelFor(expression, new { @class = "control-label" }); ;
            MvcHtmlString validation = html.ValidationMessageFor(expression);

            MvcHtmlString control = typeof (TValue).IsEnum ? html.EnumDropDownListFor(expression, new {@class = "form-control"}) : html.EditorFor(expression, new { htmlAttributes = new {@class = "form-control" } });

            var div = new TagBuilder("div")
            {
                InnerHtml =
                    label.ToString() + control + validation
            };

            div.AddCssClass("form-group");
            return MvcHtmlString.Create(div.ToString());
        }

        public static MvcHtmlString Button<TModel>(this HtmlHelper<TModel> html, string text, string url)
        {
            var button = new TagBuilder("a");
            button.MergeAttribute("href", url);
            button.MergeAttribute("class", "btn btn-primary");
            button.MergeAttribute("role", "button");
            button.SetInnerText(text);

            return MvcHtmlString.Create(button.ToString());
        }

        public static MvcHtmlString CancelButton<TModel>(this HtmlHelper<TModel> html, string url)
        {
            var button = new TagBuilder("a");
            button.MergeAttribute("href", url);
            button.MergeAttribute("class", "btn btn-default");
            button.MergeAttribute("role", "button");
            button.SetInnerText("Cancel");

            return MvcHtmlString.Create(button.ToString());
        }

        public static MvcHtmlString SubmitButton<TModel>(this HtmlHelper<TModel> html, string innerText)
        {
            var button = new TagBuilder("button");
            button.MergeAttribute("type", "submit");
            button.MergeAttribute("class", "btn btn-primary");
            button.SetInnerText(innerText);

            return MvcHtmlString.Create(button.ToString());
        }
    }
}