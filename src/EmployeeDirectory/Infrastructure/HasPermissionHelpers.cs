﻿using System;
using System.Linq;
using System.Web.Mvc;
using EmployeeDirectory.Core;

namespace EmployeeDirectory.Infrastructure
{
    public static class HasPermissionHelpers
    {


        public static bool HasDeletePermission()
        {
            return GetUsersPermissions()
                .Contains(Permission.DeleteEmployees);

        }

        public static bool HasEditPermission()
        {
            return GetUsersPermissions()
                .Contains(Permission.EditEmployees);
        }

        public static bool HasRegisterPermission()
        {
            return GetUsersPermissions()
                .Contains(Permission.RegisterEmployees);
        }

        public static bool HasSecurityPermission()
        {
            return GetUsersPermissions()
                .Contains(Permission.ManageSecurity);
        }

        public static Guid UsersIdForComparison()
        {
            var context = DependencyResolver.Current.GetService<RequestContext>();
            return context.User.Id;
        }
        public static Permission[] GetUsersPermissions()
        {
            var context = DependencyResolver.Current.GetService<RequestContext>();
            return context.Permissions;
        }
    }
}