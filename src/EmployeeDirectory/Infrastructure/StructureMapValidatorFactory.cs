﻿using System;
using System.Web.Mvc;
using FluentValidation;

namespace EmployeeDirectory.Infrastructure
{
    public class StructureMapValidatorFactory : ValidatorFactoryBase
    {
        public override IValidator CreateInstance(Type validatorType)
        {
            return DependencyResolver.Current.GetService(validatorType) as IValidator;
        }
    }
}