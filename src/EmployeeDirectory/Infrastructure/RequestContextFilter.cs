using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;
using EmployeeDirectory.Core;

namespace EmployeeDirectory.Infrastructure
{
    public class RequestContextFilter : IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            var directory = DependencyResolver.Current.GetService<DirectoryContext>();
            var context = DependencyResolver.Current.GetService<RequestContext>();
            context.Permissions = new Permission[] {};

            context.User = directory.Employees.SingleOrDefault(x => x.Username == filterContext.HttpContext.User.Identity.Name);

            if (context.User != null)
            {
                context.Permissions =
                    directory.Database.SqlQuery<Permission>(
                        @"SELECT DISTINCT [RolePermission].[Permission]
            FROM [Role]
            INNER JOIN [RolePermission] ON [RolePermission].[RoleId] = [Role].[Id]
            INNER JOIN [EmployeeRole] ON [EmployeeRole].[RoleId] = [Role].[Id]
            WHERE [EmployeeRole].EmployeeId = @EmployeeId",
                        new SqlParameter("EmployeeId", context.User.Id)).ToArray();
            }

        }
    }
}