﻿using StructureMap.Configuration.DSL;

namespace EmployeeDirectory.Infrastructure
{
    public class FluentValidatorRegistry : Registry
    {
        public FluentValidatorRegistry()
        {
            FluentValidation.AssemblyScanner.FindValidatorsInAssemblyContaining<FluentValidatorRegistry>()
                .ForEach(result =>
                    For(result.InterfaceType)
                    .Use(result.ValidatorType));
        }
    }
}