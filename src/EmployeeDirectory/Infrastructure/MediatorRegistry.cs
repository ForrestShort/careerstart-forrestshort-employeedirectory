﻿using EmployeeDirectory.Controllers;
using MediatR;
using StructureMap.Configuration.DSL;

namespace EmployeeDirectory.Infrastructure
{
    public class MediatorRegistry : Registry
    {
        public MediatorRegistry ()
        {
            Scan(scanner =>
            {
                scanner.AssemblyContainingType<EmployeeController>();
                scanner.ConnectImplementationsToTypesClosing(typeof (IRequestHandler<,>));
                scanner.ConnectImplementationsToTypesClosing(typeof (IAsyncRequestHandler<,>));
                scanner.ConnectImplementationsToTypesClosing(typeof (INotificationHandler<>));
                scanner.ConnectImplementationsToTypesClosing(typeof (IAsyncNotificationHandler<>));

            });
                
            For<SingleInstanceFactory>().Use<SingleInstanceFactory>(ctx => t => ctx.GetInstance(t));
            For<MultiInstanceFactory>().Use<MultiInstanceFactory>(ctx => t => ctx.GetAllInstances(t));
            For<IMediator>().Use<Mediator>();
        }
    }
}