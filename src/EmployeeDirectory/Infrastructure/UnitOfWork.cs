﻿using System.Web.Mvc;
using EmployeeDirectory.Core;

namespace EmployeeDirectory.Infrastructure
{
    public class UnitOfWork : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var context = DependencyResolver.Current.GetService<DirectoryContext>();

            context.BeginTransaction();
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var context = DependencyResolver.Current.GetService<DirectoryContext>();

            context.CloseTransaction(filterContext.Exception);

        }
    }
}