﻿using System.Web.Security;

namespace EmployeeDirectory.Infrastructure
{
    public interface IAuthenticationService
    {
        void SetAuthCookie(string username);
        void SignOut();
    }

    public class AuthenticationService : IAuthenticationService
    {
        public void SetAuthCookie(string username)
        {
            FormsAuthentication.SetAuthCookie(username, false);
        }

        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }
    }
}