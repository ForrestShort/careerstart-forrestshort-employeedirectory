﻿using System.Collections.Generic;
using EmployeeDirectory.Core;

namespace EmployeeDirectory.Infrastructure
{
    public class RequestContext
    {
        public Employee User { get; set; }
        public Permission[] Permissions { get; set; }
    }
}