using System;
using StructureMap.Graph;

namespace EmployeeDirectory.DependencyResolution
{
    using StructureMap;

    public static class IoC
    {
        private static readonly Lazy<IContainer> _container = new Lazy<IContainer>(Initialize, true);

        public static IContainer Container
        {
            get { return _container.Value; }
        }

        private static IContainer Initialize()
        {
            return new Container(cfg => cfg.Scan(scan =>
            {
                scan.TheCallingAssembly();
                scan.LookForRegistries();
                scan.WithDefaultConventions();
                scan.With(new ControllerConvention());
            }));
        }
    }
}