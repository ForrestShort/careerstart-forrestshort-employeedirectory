﻿using System.Web.Mvc;
using System.Web.Routing;
using AutoMapper;
using EmployeeDirectory.App_Start;
using EmployeeDirectory.Extensions;
using EmployeeDirectory.Infrastructure;
using FluentValidation.Mvc;

namespace EmployeeDirectory
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new FeatureViewLocationRazorViewEngine());

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            AutoMapperBootstrapper.Initialize();
            Mapper.AssertConfigurationIsValid();

            FluentValidationModelValidatorProvider.Configure(provider =>
            {
                provider.ValidatorFactory = new StructureMapValidatorFactory();
            });
        
        }

    }
}
