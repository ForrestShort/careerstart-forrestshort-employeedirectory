﻿using System.Web.Mvc;

namespace EmployeeDirectory.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return RedirectToAction("Login", "Account");
        }
    }
}