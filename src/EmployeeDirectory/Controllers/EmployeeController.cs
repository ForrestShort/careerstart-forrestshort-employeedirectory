﻿using System.Web.Mvc;
using EmployeeDirectory.Core;
using EmployeeDirectory.Features.Employee;
using EmployeeDirectory.Infrastructure;
using MediatR;

namespace EmployeeDirectory.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly IMediator _mediator;

        public EmployeeController(EmployeeIndexQueryHandler indexQuery,
            IMediator mediator)
        {
            _mediator = mediator;
        }

        public ActionResult Index(EmployeeIndexQuery query)
        {
            var model = _mediator.Send(query);

            return View(model);
        }

        [RequirePermissions(Permission.RegisterEmployees)]
        public ActionResult Create()
        {
            return View();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        [RequirePermissions(Permission.RegisterEmployees)]
        public ActionResult Create(CreateEmployeeForm form)
        {
            if (ModelState.IsValid)
            {
                _mediator.Send(form);

                return RedirectToAction("Index");
            }

            return View();
        }

        [RequirePermissions(Permission.EditEmployees)]
        public ActionResult Edit(EditEmployeeQuery query)
        {
            var model = _mediator.Send(query);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [RequirePermissions(Permission.EditEmployees)]
        public ActionResult Edit(EditEmployeeForm form)
        {
            if (ModelState.IsValid)
            {
                _mediator.Send(form);

                return RedirectToAction("Index");
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [RequirePermissions(Permission.DeleteEmployees)]
        public ActionResult Delete(DeleteEmployeeCommand command)
        {
            _mediator.Send(command);

            return RedirectToAction("Index");
        }

        [RequirePermissions(Permission.ManageSecurity)]
        public ActionResult Roles(SetRolesQuery query)
        {
            var model = _mediator.Send(query);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [RequirePermissions(Permission.ManageSecurity)]
        public ActionResult Roles(SetRolesForm form)
        {
            if (ModelState.IsValid)
            {
                _mediator.Send(form);

                return RedirectToAction("Index");
            }

            return View();
        }

        public ActionResult EditProfile(EditProfileQuery query)
        {
            var model = _mediator.Send(query);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProfile(EditProfileForm form)
        {
            if (ModelState.IsValid)
            {
                _mediator.Send(form);

                return RedirectToAction("Index");
            }

            return View();
        }
    }
}