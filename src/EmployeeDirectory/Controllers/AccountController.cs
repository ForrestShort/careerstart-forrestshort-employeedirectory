﻿using System.Web.Mvc;
using EmployeeDirectory.Features.Account;
using EmployeeDirectory.Infrastructure;
using MediatR;

namespace EmployeeDirectory.Controllers
{
    public class AccountController : Controller
    {
        private readonly IMediator _mediator;

        public AccountController(IMediator mediator)
        {
            _mediator = mediator;
        }
        
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Login(LoginForm form)
        {
            if (ModelState.IsValid)
            {
                _mediator.Send(form);

                return RedirectToAction("Index", "Employee");
            }

            return View();
        }

        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(ChangePasswordForm form)
        {
            if (ModelState.IsValid)
            {
                _mediator.Send(form);

                return RedirectToAction("Index", "Employee");
            }

            return View();
        }

        public ActionResult Logout()
        {
            var authenticate = new AuthenticationService();
            authenticate.SignOut();
            return RedirectToAction("Login", "Account");
        }
    }
}