﻿using System.Web.Mvc;
using EmployeeDirectory.Infrastructure;

namespace EmployeeDirectory
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new RequestContextFilter());
            filters.Add(new AuthorizeAttribute());
            filters.Add(new UnitOfWork());
            filters.Add(new HandleErrorAttribute());
        }
    }
}
