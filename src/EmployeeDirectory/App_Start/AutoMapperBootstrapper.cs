﻿using System;
using System.Linq;
using AutoMapper;

namespace EmployeeDirectory.App_Start
{
    public class AutoMapperBootstrapper
    {
        //Lazy<T> is usually used to create a single instance of some type, even if multiple threads attempt to access it at the same time.

        //Although there is no AutoMapper object to create, we similarly want to ensure that initialization happens in a thread safe way.

        //Therefore, we lazily construct an instance of this class and then throw it away, so that we can ensure the initialization happens

        //safely without having to do manual locks ourselves.


        private static readonly Lazy<AutoMapperBootstrapper> _bootstrapper =
            new Lazy<AutoMapperBootstrapper>(InternalInitialize);


        public static void Initialize()
        {
            var bootstrapper = _bootstrapper.Value;
        }


        private AutoMapperBootstrapper()
        {
        }


        private static AutoMapperBootstrapper InternalInitialize()
        {
            var profiles = typeof (AutoMapperBootstrapper)
                .Assembly
                .GetTypes()
                .Where(type => type.IsSubclassOf(typeof (Profile)))
                .Select(Activator.CreateInstance)
                .Cast<Profile>()
                .ToArray();


            Mapper.Initialize(cfg =>
            {
                foreach (var profile in profiles)

                    cfg.AddProfile(profile);


                cfg.Seal();
            });


            return new AutoMapperBootstrapper();
        }
    }
}
